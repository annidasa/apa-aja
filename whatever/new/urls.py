from django.urls import path
import new
from .views import *

urlpatterns = [
    path('', index, name='home'),
    path('addstatus/', add_status, name='addstatus'),
]