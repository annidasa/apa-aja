from django import forms
from .models import Status
# Create your models here.


class Status_Form(forms.ModelForm):
    class Meta:
        model = Status
        fields = ('status',)